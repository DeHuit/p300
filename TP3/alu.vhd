LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY CombinationalTools;
USE CombinationalTools.bus_mux_pkg.ALL;
LIBRARY SequentialTools;
USE SequentialTools.ALL;

ENTITY ALU IS
    generic (
        data_size : INTEGER := 32;
        mux_sel_size : INTEGER :=4;
        shift_size : INTEGER := 5
    );
	PORT
	(
		A : IN STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
		B : IN STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
		sel : IN STD_LOGIC_VECTOR(mux_sel_size - 1 DOWNTO 0);
		Enable_V : IN STD_LOGIC;
		ValDec : IN STD_LOGIC_VECTOR(shift_size - 1 DOWNTO 0);
		Slt : IN STD_LOGIC;
		CLK : IN STD_LOGIC;
		Res : OUT STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
		N : OUT STD_LOGIC;
		Z : OUT STD_LOGIC;
		C : OUT STD_LOGIC;
		V : OUT STD_LOGIC
	);
END ENTITY ALU;

architecture alu_arc of alu is
    signal mux_bus : bus_mux_array(2**(mux_sel_size-1)-1 downto 0)(data_size -1 downto 0);
    signal mux_sel : STD_LOGIC_VECTOR(mux_sel_size - 2 downto 0);
    signal pos : STD_LOGIC;
    signal C31_alu : STD_LOGIC;
    signal C30_alu : STD_LOGIC;
    signal B_to_alu : STD_LOGIC_VECTOR(data_size - 1 downto 0);
    signal adder_result : STD_LOGIC_VECTOR(data_size - 1 downto 0);
    signal zero_vector : STD_LOGIC_VECTOR(data_size -1 downto 0) := (others=>'0');
    signal ones_vector : STD_LOGIC_VECTOR(data_size -1 downto 0) := (others=>'1');
begin
    -- MUX
    mux_sel <= sel(sel'length-2 downto 0);
    mux : entity CombinationalTools.mux
            generic map (mux_sel_size-1, data_size) 
            port map (input=>mux_bus, 
                      sel_input=>mux_sel, 
                      output=>Res);

    mux_bus(0) <= A and B;
    mux_bus(1) <= A or B;
    mux_bus(2) <= adder_result;
    mux_bus(3) <= (0=>pos, others=>'0');
    mux_bus(4) <= A nor B;
    mux_bus(5) <= A xor B;

    LSR : entity work.barrel_shifter
    generic map (shift_size, data_size)
    port map (
        input => B,
        shift_amount => ValDec,
        LeftRight => '1',
        LogicArith => '0',
        ShiftRotate => '0',
        output => mux_bus(6) );

    LSL : entity work.barrel_shifter
	        generic map (shift_size, data_size)
            port map (
                input => B,
                shift_amount => ValDec,
                LeftRight=> '0',
                LogicArith => '0',
                ShiftRotate => '0',
                output => mux_bus(7) 
                );
    --------------------------------
    ------- SERVICE SIGNALS --------
    --------------------------------

    --POS signal
    pos <= ((C31_alu xor sel(mux_sel_size-1))
                             and 
            not Enable_V) 
                                    or 
            (Enable_V        and 
            ((C30_alu xor C31_alu) xor adder_result(data_size-1)));

    -- ADDER
    B_to_alu <= B when sel(sel'length - 1) = '0' 
                else (B xor ones_vector);
    CALC : entity work.adder 
            generic map(data_size)
            port map (
                A => A,
                B => B_to_alu,
                S => adder_result, 
                C0 => sel(3), -- carry in to 0 by default
                C30 => C30_alu, --signed carry out
                C31 => C31_alu -- unsigned carry out
            );

    -- Control signals output (as Gate d latches)
    C_sig : entity work.GateD port map (clk => clk,d => C31_alu xor sel(3), q => C);
    V_sig : entity work.GateD port map (clk => clk,d =>Enable_V and not Slt and (C31_alu xor C30_alu), q => C);
    N_sig : entity work.GateD port map (clk => clk,d => adder_result(data_size-1), q => N);
    Z_sig : entity work.GateD port map (clk => clk,d => and (adder_result nor adder_result), q => Z);

end alu_arc; 