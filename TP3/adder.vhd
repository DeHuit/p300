LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY Adder IS
  generic (
          data_size : INTEGER := 32
          );
	PORT
	(
        A : IN STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0); -- data 1
        B : IN STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0); -- data 2
        S : OUT STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0); -- data out
        C0 : IN STD_LOGIC;
        C30 : OUT STD_LOGIC;
        C31 : OUT STD_LOGIC
	);
END ENTITY Adder;


architecture adder_arc of Adder is  
    constant vec : STD_LOGIC := '1';
    signal inter31 : STD_LOGIC_VECTOR(data_size-1 DOWNTO 0) := (others=>'0');
    signal inter32 : STD_LOGIC_VECTOR(data_size DOWNTO 0) := (others=>'0'); -- easy to detect unsigned carry out! just add extra carry bit!
    signal n : STD_LOGIC;
begin 
    n <= vec xor vec;
    S <= std_logic_vector(unsigned(A) + unsigned(B)) when C0 = '0' else
         std_logic_vector(unsigned(A) + unsigned(B)+1); -- substraction

    
    inter31 <= std_logic_vector(unsigned("0" & A(data_size-2 downto 0)) + unsigned("0" & B(data_size-2 downto 0)));
    inter32 <= std_logic_vector(unsigned("0" & A) + unsigned("0" & B));


    C30 <= '1' when inter31(data_size - 1)  = '1' else '0';
    C31 <= '1' when inter32(data_size)  = '1' else '0';

end adder_arc; -- adder_arc