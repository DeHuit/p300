LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY barrel_shifter IS
	GENERIC 
	(
		shift_size : INTEGER := 5;
		shifter_width : INTEGER := 32
	);
	PORT
	(
		input : IN STD_LOGIC_VECTOR(shifter_width - 1 DOWNTO 0);
		shift_amount : IN STD_LOGIC_VECTOR(shift_size-1 DOWNTO 0);
		LeftRight : IN STD_LOGIC;
		LogicArith : IN STD_LOGIC;
		ShiftRotate : IN STD_LOGIC;
		output : OUT STD_LOGIC_VECTOR(shifter_width - 1 DOWNTO 0)
	);
END ENTITY barrel_shifter;


architecture barrel_shifter_arc of barrel_shifter is
begin
	-- Idea : concatanet 2 vectors
	-- IDea 2 : generate larger vector
	--LSL >>  111010 <<2 101000 || 111010 <<2R 101011 o(1) = i(5) o(0) = i(4)
	--LSR >>  111010 >>2 001110 || 111010 <<2R 101110 o(5) = i(1) o(4) = i(0)
	GET_OUT : for i in output'range generate
		output(i) <= --Right shift
					'0'																 when (LeftRight = '1') and (ShiftRotate ='0') and (LogicArith = '0') and (shifter_width - i <= to_integer(unsigned(shift_amount))) else -- First N bits go 0
					input(shifter_width-1)											 when (LeftRight = '1') and (ShiftRotate ='0') and (LogicArith = '1') and (shifter_width - i <= to_integer(unsigned(shift_amount))) else --First N bitss go a sign
					input(to_integer(unsigned(shift_amount) - (shifter_width - i)))  when (LeftRight = '1') and (ShiftRotate ='1') and (shifter_width - i <= to_integer(unsigned(shift_amount))) else -- First N bits are rotated
					input(i + to_integer(unsigned(shift_amount))) 					 when (LeftRight = '1') and shifter_width - i > to_integer(unsigned(shift_amount)) else -- copying rest of them to the end
					--Left shift
					'0' 															 when (LeftRight = '0') and (ShiftRotate = '0') and (i - to_integer(unsigned(shift_amount))) < 0 else -- Last N bits go 0
					input((shifter_width) - (to_integer(unsigned(shift_amount)) -i)) when (LeftRight = '0') and (ShiftRotate = '1') and (i - to_integer(unsigned(shift_amount))) < 0 else -- Last N bits are rotated
					input(i - to_integer(unsigned(shift_amount))) 					 when (LeftRight = '0') and i - to_integer(unsigned(shift_amount)) >= 0 else -- Copying the rest of bits to the begining
					'Z';
	end generate; -- GEN_OUT
end barrel_shifter_arc; -- decoder_arc