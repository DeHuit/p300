LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
LIBRARY CombinationalTools;
USE CombinationalTools.all;
USE CombinationalTools.bus_mux_pkg.ALL;

entity test_bank is
end test_bank;

architecture behavior of test_bank is
    constant TIMEOUT 	: time := 100 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de clock
    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
    -- definition des signal
    constant address_size : integer := 2;
    constant data_size : integer := 4;
    -- entity inputs
    signal sr0 : STD_LOGIC_VECTOR(address_size - 1 DOWNTO 0);
    signal d0 :  STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
    signal sr1 : STD_LOGIC_VECTOR(address_size - 1 DOWNTO 0);
    signal d1 :  STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
	signal dr :  STD_LOGIC_VECTOR(address_size - 1 DOWNTO 0);
	signal din : STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
    signal wr :  STD_LOGIC;
begin    
    --------------------------
    -- definition de l'horloge et timeout
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition
    rb : entity WORK.RegisterBank
                        generic map (address_size, data_size) 
                        port map (sr0, d0, sr1, d1, dr, din, wr, clk);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF std_logic_vector (3 downto 0);
        constant TdV : TableDeVerite :=("0000", "0001", "0010", "1111");
    begin
        FOR I IN TdV'RANGE LOOP
            sr0 <= "01";
            sr1 <= "10";
            din <= "1111";
            -- write to register 1
            dr <= "01";
            wr <= '0';
            WAIT FOR clkpulse;
            wr <= '1';
            WAIT FOR  clkpulse;
            -- write to register 0
            dr <= "10";
            wr <= '0';
            WAIT FOR  clkpulse;
            wr <= '1';
            WAIT FOR  clkpulse * 4;
            -- writing and reading 0 (should give 0 in both cases)
            sr0 <= "00";
            dr <= "00";
            wr <= '0';
            WAIT FOR  clkpulse;
            wr <= '1';
            WAIT FOR  clkpulse;
            wait;   
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;