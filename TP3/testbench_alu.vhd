LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity test_alu is
end test_alu;

architecture behavior of test_alu is
    constant TIMEOUT 	: time := 500 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de ressources externes
    signal E_RST,E_CLK	 	: std_logic;
    -- generic
    constant data_size : INTEGER := 32;
    constant mux_sel_size : INTEGER :=4;
    constant shift_size : INTEGER := 5;
    -- definition des signal
    signal A : STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
	signal B : STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
    signal sel : STD_LOGIC_VECTOR(mux_sel_size - 1 DOWNTO 0);
    signal Enable_V : STD_LOGIC;
    signal ValDec : STD_LOGIC_VECTOR(shift_size - 1 DOWNTO 0);
    signal Slt : STD_LOGIC;
    signal Res : STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
    signal N : STD_LOGIC;
    signal Z : STD_LOGIC;
    signal C : STD_LOGIC;
    signal V : STD_LOGIC;
    -- Clock
    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
begin    
    --------------------------
    -- definition de l'horloge et timeout
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition of decoder
    alu : entity WORK.alu
                        generic map (data_size, mux_sel_size, shift_size) 
                        port map (A, B, sel, Enable_V, ValDec, Slt, CLK, res, N, Z, C, V);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        constant shifting_value : std_logic_vector(ValDec'range) := "00011";
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF std_logic_vector(data_size-1 downto 0);
        constant TdV : TableDeVerite :=((others=>'0'), (0=>'1', others=>'0'), (others=>'1'), ("1111", others=>'0'));
    begin
        -- WARNING
        -- Exostive tests cause VHDL bug that can not be resolved (idk why ¯\_(ツ)_/¯)
        -- So only non-exaustive test are allowed
        ValDec <= shifting_value;
        -- A loop
        FOR I IN TdV'RANGE LOOP
        --FOR I IN 0 to 2**(data_size)-1 LOOP
            A <= TdV(I);
            --A <= std_logic_vector(to_unsigned(I, A'length));
            -- B loop
            FOR J IN TdV'RANGE LOOP
            --FOR J IN 0 to 2**(data_size)-1 LOOP
                B <= TdV(J);
                --B <= std_logic_vector(to_unsigned(J, B'length));
                -- sel loop
                FOR int_sel IN 0 to 2**(mux_sel_size)-1 LOOP
                    sel <= std_logic_vector(to_unsigned(int_sel, sel'length));
                    Enable_V <= '0';
                    Slt <= '0';
                    wait for clkpulse;
                    Slt <= '1';
                    wait for clkpulse;
                    Enable_V <= '1';
                    Slt <= '0';
                    wait for clkpulse;   
                    Slt <= '1';
                    wait for clkpulse;             
                END LOOP;
            END LOOP;
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;