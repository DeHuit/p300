library IEEE;
use IEEE.std_logic_1164.all;

entity GateD is
    port(
        clk : IN std_logic;
        d : IN std_logic;
        q : OUT std_logic
    );
end entity;

architecture GateD_arc of GateD is
begin
    write : process( clk )
    begin
        q <= d;
    end process ; -- write
end GateD_arc ; -- GateD_arc