-- Definition des librairies
library IEEE;
library WORK;

-- Definition des portee d'utilisation
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
-- use IEEE.std_logic_unsigned.all;	-- sinon conflit avec notre fonction "+" !!



entity test_decoder is
end test_decoder;

architecture behavior of test_decoder is
    constant TIMEOUT 	: time := 100 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de ressources externes
    signal E_RST,E_CLK	 	: std_logic;
    -- definition des signal
    signal e_input : std_logic_vector(2 - 1 downto 0);
    signal e_output : std_logic_vector((2**2)-1 downto 0);

    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
begin    
    --------------------------
    -- definition de l'horloge et timeput
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition of decoder
    dec : entity WORK.decoder
                            generic map (2) 
                            port map (input=>e_input, output => e_output);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF STD_LOGIC_VECTOR(1 DOWNTO 0);
        constant TdV : TableDeVerite :=("00", "01", "10", "11");
    begin
        FOR I IN TdV'RANGE LOOP
            e_input <= TdV(I);
            WAIT FOR 5 ns;
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;