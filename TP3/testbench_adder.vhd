LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity test_adder is
end test_adder;

architecture behavior of test_adder is
    constant TIMEOUT 	: time := 100 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de ressources externes
    signal E_RST,E_CLK	 	: std_logic;
    -- generic
    constant data_size : integer := 4;
    -- definition des signal
    signal A :  STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0); -- data 1
    signal B :  STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0); -- data 2
    signal S :  STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0); -- data out
    signal C0 :  STD_LOGIC;
    signal C30 :  STD_LOGIC;
    signal C31 :  STD_LOGIC;
    -- Clock
    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
begin    
    --------------------------
    -- definition de l'horloge et timeout
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition of decoder
    dec : entity WORK.adder
                        generic map (data_size) 
                        port map (A, B, S, C0, C30, C31);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF std_logic_vector (data_size-1 downto 0);
        constant TdV_A : TableDeVerite :=("1100", "0100", "1000", "1000");
        constant TdV_B : TableDeVerite :=("1100", "1000", "0100", "1000");
    begin
        FOR I IN TdV_A'RANGE LOOP
            A <= TdV_A(I);
            FOR J IN TdV_B'RANGE LOOP
                B <= TdV_A(J);
                C0 <= '0';
                WAIT FOR clkpulse;
                C0 <= '1';
                WAIT FOR clkpulse;
            END LOOP;
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;