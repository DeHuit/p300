LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity test_shifter is
end test_shifter;

architecture behavior of test_shifter is
    constant TIMEOUT 	: time := 100 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de clock
    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
    -- definition des signal
    constant shift_size : INTEGER := 2;
    constant shifter_width : INTEGER := 4;
    -- entity e_inpus
    signal e_input :  STD_LOGIC_VECTOR(shifter_width - 1 DOWNTO 0);
	signal e_shift_amount :  STD_LOGIC_VECTOR(shift_size-1 DOWNTO 0);
	signal e_LeftRight :  STD_LOGIC;
	signal e_LogicArith :  STD_LOGIC;
	signal e_ShiftRotate :  STD_LOGIC;
	signal e_output :  STD_LOGIC_VECTOR(shifter_width - 1 DOWNTO 0);
begin    
    --------------------------
    -- definition de l'horloge et timeout
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition
    bs : entity WORK.barrel_shifter
                        generic map (shift_size, shifter_width) 
                        port map (e_input, e_shift_amount, e_LeftRight, e_LogicArith, e_ShiftRotate, e_output);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF std_logic_vector (3 downto 0);
        TYPE TableDesAddresss IS ARRAY(0 TO 3) OF std_logic_vector (1 downto 0);
        constant TdV : TableDeVerite :=("1100", "0111", "0010", "1111");
        constant TdA : TableDesAddresss :=("00", "01", "10", "11");
    begin
        e_LeftRight <= '1';
        e_LogicArith <= '1';
        e_ShiftRotate <= '0';
        FOR J IN TdA'RANGE LOOP
            e_shift_amount <= TdA(J);
            FOR I IN TdV'RANGE LOOP
                e_input <= TdV(i);
                e_LogicArith <= '0';
                wait for clkpulse;
                e_LogicArith <= '1';
                wait for clkpulse;
            END LOOP ;
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;