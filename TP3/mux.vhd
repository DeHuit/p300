LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

PACKAGE bus_mux_pkg IS
	TYPE bus_mux_array IS ARRAY(NATURAL RANGE<>) OF STD_LOGIC_VECTOR;
END PACKAGE bus_mux_pkg;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.bus_mux_pkg.ALL;

entity mux is
    generic (
        mux_size : INTEGER := 2;
        mux_width : INTEGER :=32
    );
  port (
    input : in bus_mux_array((2**mux_size)-1 downto 0)(mux_width-1 downto 0);
    sel_input : in std_logic_vector(mux_size - 1 downto 0);
    output : out std_logic_vector(mux_width-1 downto 0)
  );
end mux;

architecture mux_arc of mux is    
begin  
    output <= input(to_integer(unsigned(sel_input)));
end mux_arc; -- decoder_arc
