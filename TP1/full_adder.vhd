library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TP1EX1 is
 Port ( E1, E0 : IN STD_LOGIC;
	S3, S2, S1, S0 : OUT STD_LOGIC);
end TP1EX1;

architecture fdd_TP1EX1 of TP1EX1 is
	SIGNAL S : STD_LOGIC_VECTOR(3 downto 0);
	SUBTYPE selector is STD_LOGIC_VECTOR(1 downto 0);
begin
	(S3, S2, S1, S0) <= S;
with selector'(E1 & E0) SELECT
	S <= "0001" when "00",
	     "0010" when "01",
	     "0100" when "10",
	     "1000" when "11",
	     "0000" when others;
end fdd_TP1EX1;
