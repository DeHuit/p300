LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL

ENTITY test IS
END test;

ARCHITECTURE behavior OF test IS 

 SIGNAL I1, I0, O3, O2, O1, O0 : STD_LOGIC := '0';
BEGIN
	fddTP1EX1 : ENTITY.WORK.TP1EX1 PORT MAP (I1, I0, O3, O2, O1, O0);
	I1 <= '0', '1' after 20 ns, 'U' after 40 ns, '0' after 50 ns;
	I0 <= '0', '1' after 10 ns, '0' after 20 ns, '1' after 30 ns, 'U' after 40 ns, '0' after 50 ns;
END ARCHITECTURE;
