LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.bus_mux_pkg.all;

entity test_reg is
end test_reg;

architecture behavior of test_reg is
    constant TIMEOUT 	: time := 100 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de clock
    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
    -- definition des signal
    constant reg_size : integer := 4;
    signal e_wr : std_logic;
    signal e_in : std_logic_vector(reg_size - 1 downto 0);
    signal e_out : std_logic_vector(reg_size - 1 downto 0);
begin    
    --------------------------
    -- definition de l'horloge et timeout
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition of decoder
    dec : entity WORK.reg
                        generic map (reg_size) 
                        port map (data_in=>e_in, wr=>e_wr, data_out => e_out);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF std_logic_vector (3 downto 0);
        constant TdV : TableDeVerite :=("0000", "0001", "0010", "1111");
    begin
        FOR I IN TdV'RANGE LOOP
            e_wr <= '0';
            e_in <= TdV(I);
            WAIT FOR 5 ns;
            e_wr <= '1';
            WAIT FOR 5 ns;
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;