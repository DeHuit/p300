library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity decoder is
    generic (
        dec_size : INTEGER := 2
    );
  port (
    input : in std_logic_vector(dec_size - 1 downto 0);
    output : out std_logic_vector((2**dec_size)-1 downto 0)
  );
end decoder;

architecture decoder_arc of decoder is    
begin  
    GEN_OUT : for I in 0 to (2 ** dec_size)-1 generate
        output(I) <= '1' when i = unsigned(input) else '0';
    end generate GEN_OUT;
end decoder_arc; -- decoder_arc