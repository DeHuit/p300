LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY CombinationalTools;
USE CombinationalTools.all;
USE CombinationalTools.bus_mux_pkg.ALL;

LIBRARY SequentialTools;
USE SequentialTools.ALL;

ENTITY RegisterBank IS
  generic (
          address_size : INTEGER := 4;
          data_size : INTEGER := 32
          );
	PORT
	(
    --
		source_register_0 : IN STD_LOGIC_VECTOR(address_size - 1 DOWNTO 0);
		data_out_0 : OUT STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
		source_register_1 : IN STD_LOGIC_VECTOR(address_size - 1 DOWNTO 0);
		data_out_1 : OUT STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
		destination_register : IN STD_LOGIC_VECTOR(address_size - 1 DOWNTO 0);
		data_in : IN STD_LOGIC_VECTOR(data_size - 1 DOWNTO 0);
		write_register : IN STD_LOGIC;
		clk : IN STD_LOGIC
	);
END ENTITY RegisterBank;


architecture bank_arc of RegisterBank is
  --signal mux_bux : bus_mux_array(2**mux_size)-1 downto 0)(mux_width-1 downto 0)
  signal demux_bus : std_logic_vector((2**address_size)-1 downto 0) := (others=>'0');
  signal wr_inputs : std_logic_vector((2**address_size)-1 downto 0) := (others=>'0');
  signal mux_bus   : bus_mux_array((2**address_size)-1 downto 0)(data_size - 1 downto 0);
  signal zero_wire  : std_logic_vector(data_size - 1 downto 0) := (others=>'0');
begin  
  -- read data
  demux : entity CombinationalTools.decoder
          generic map (address_size) 
          port map (input=>destination_register,output=>demux_bus);

  RegArray : for i in (2**address_size)-1 downto 1 generate
    regx : entity SequentialTools.reg generic map (data_size) 
                  port map (wr=>wr_inputs(i), data_in=>data_in, data_out=>mux_bus(i));
  end generate ; -- RegArray
  
  reg_zero : entity SequentialTools.reg generic map (data_size) 
             port map (wr=>'0', data_in=>zero_wire, data_out=>mux_bus(0));

  WR : for i in wr_inputs'range generate
    wr_inputs(i) <= write_register and clk and demux_bus(i);
  end generate ; -- WR

  -- READING
  mux0 : entity CombinationalTools.mux
            generic map (address_size, data_size) 
            port map (input=>mux_bus, 
                      sel_input=>source_register_0, 
                      output=>data_out_0);

  mux1 : entity CombinationalTools.mux
            generic map (address_size, data_size) 
            port map (input=>mux_bus, 
                      sel_input=>source_register_1, 
                      output=>data_out_1);                
end bank_arc; -- decoder_arc
