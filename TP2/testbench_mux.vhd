LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.bus_mux_pkg.all;

entity test_mux is
end test_mux;

architecture behavior of test_mux is
    constant TIMEOUT 	: time := 100 ns; -- timeout de la simulation
    -- definition de constantes
    constant clkpulse : Time := 5 ns; -- 1/2 periode horloge
    -- definition de ressources externes
    signal E_RST,E_CLK	 	: std_logic;
    -- generic
    constant mux_size : integer := 2;
    constant mux_width : integer := 4;
    -- definition des signal
    signal e_input :  bus_mux_array((mux_size**2)-1 downto 0)(mux_width-1 downto 0);
    signal e_output : std_logic_vector(mux_width-1 downto 0);
    signal e_nsel : std_logic_vector(mux_size - 1 downto 0);

    signal clk : std_logic := '0';
    signal clk_en : std_logic := '1';
begin    
    --------------------------
    -- definition de l'horloge et timeout
    clk <= (NOT clk) and clk_en after clkpulse;
    clk_en <= '0' after TIMEOUT;

    -- definition of decoder
    dec : entity WORK.mux
                        generic map (mux_size, mux_width) 
                        port map (input=>e_input, sel_input=>e_nsel, output => e_output);
    -----------------------------
    -- debut sequence de test
    P_TEST: process
        TYPE TableDeVerite IS ARRAY(0 TO 3) OF std_logic_vector (mux_size-1 downto 0);
        constant TdV : TableDeVerite :=("00", "01", "10", "11");
    begin
        e_input <= (0 => "0001", 
                    1 => "0011", 
                    2 => "0111", 
                    3 => "1111");
        FOR I IN TdV'RANGE LOOP
            e_nsel <= TdV(I);
            WAIT FOR clkpulse;
        END LOOP;
        WAIT;
    end process P_TEST;
end behavior;