LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity reg is
    generic (
        reg_size : integer := 32
    );
  port (
    wr : in std_logic;
    data_in : in std_logic_vector(reg_size - 1 downto 0);
    data_out : out std_logic_vector(reg_size - 1 downto 0)
  );
end reg;

architecture reg_arc of reg is
  signal mem : std_logic_vector(reg_size-1 downto 0) := (others=> '0');
begin  

  write : process( wr )
  begin
    if wr = '1' then
      mem <= data_in;
    end if ;
  end process ; -- write
  
  data_out <= mem;

end reg_arc; -- decoder_arc
